<?php

/**
 * @package     Nameless Vocabulary Trainer
 * @copyright	Copyright (C) 2019 TASK 3 CROWDFEST TEAM. All rights reserved.
 */
require_once ('../adodb5/adodb.inc.php');
require_once 'config.php';
require_once 'conceptnet_functions.php';

/* function update_dataset_with_timestamp() {
  $cfg = new sc_config();
  $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
  $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
  $db->execute("set names 'utf8'");
  $db->debug = $cfg->db_debug;
  $query = "SELECT a.eid,a.uid,a.exercise_type,a.response FROM dataset as a";
  $rs = $db->Execute($query);
  while ($row = $rs->FetchNextObject(false)) {
  $query2 = "SELECT a.id,a.timestamp FROM tbl_eval_close as a WHERE a.eid='" . $row->eid . "' and a.uid='" . $row->uid . "' and a.response='" . $row->response . "'";

  $rs2 = $db->Execute($query2);
  $counter=0;
  while ($row2 = $rs2->FetchNextObject(false)) {
  $counter++;
  $query3="UPDATE dataset SET timestamp='".$row2->timestamp."',num=".$counter." WHERE eid='" . $row->eid . "' and uid='" . $row->uid . "' and response='" . $row->response . "'";
  //echo $query3;
  $rs3 = $db->Execute($query3);

  }
  }
  } */

function calculate_accuracy_closed() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    //These are the 5 top contributors of closed questions
    //telegram_889914726
    //telegram_875077016
    //telegram_1054859252
    //telegram_915280822
    //telegram_1065149653
    $query = "SELECT a.eid,a.points,b.exercise_type FROM tbl_eval_close as a INNER JOIN tbl_exercises_close as b on a.eid=b.id where a.uid in('telegram_889914726','telegram_875077016','telegram_1054859252','telegram_915280822','telegram_1065149653') ORDER BY RAND() LIMIT 500";
    $rs = $db->Execute($query);
    $number_of_answers = 0;
    $number_of_answers_type1 = 0;
    $number_of_answers_type2 = 0;
    $number_of_answers_type3 = 0;
    $number_of_correct_answers = 0;
    $number_of_correct_answers_type1 = 0;
    $number_of_correct_answers_type2 = 0;
    $number_of_correct_answers_type3 = 0;
    while ($row = $rs->FetchNextObject(false)) {
        $number_of_answers++;
        if ($row->points === '1') {
            $number_of_correct_answers++;
        }
        if ($row->exercise_type === '1') {
            $number_of_answers_type1++;
            if ($row->points === '1') {
                $number_of_correct_answers_type1++;
            }
        }
        if ($row->exercise_type === '2') {
            $number_of_answers_type2++;
            if ($row->points === '1') {
                $number_of_correct_answers_type2++;
            }
        }
        if ($row->exercise_type === '3') {
            $number_of_answers_type3++;
            if ($row->points === '1') {
                $number_of_correct_answers_type3++;
            }
        }
    }
    echo "Total accuracy=(" . $number_of_correct_answers . ")/(" . $number_of_answers . ")=" . ($number_of_correct_answers * 100) / $number_of_answers . "%<br />";
    echo "Total accuracy_type1=(" . $number_of_correct_answers_type1 . ")/(" . $number_of_answers_type1 . ")=" . ($number_of_correct_answers_type1 * 100) / $number_of_answers_type1 . "%<br />";
    echo "Total accuracy_type2=(" . $number_of_correct_answers_type2 . ")/(" . $number_of_answers_type2 . ")=" . ($number_of_correct_answers_type2 * 100) / $number_of_answers_type2 . "%<br />";
    echo "Total accuracy_type3=(" . $number_of_correct_answers_type3 . ")/(" . $number_of_answers_type3 . ")=" . ($number_of_correct_answers_type3 * 100) / $number_of_answers_type3 . "%<br />";
}

function process_phrasal_verbs($subprocess = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    switch ($subprocess) {
        case 'get_canonical': {
                $query = "SELECT id,pv FROM B1_phrasal_verbs where id>0";
                $rs = $db->Execute($query);
                while ($row = $rs->FetchNextObject(false)) {
                    $upd_cform = conceptnet_search_for_proper_uri($row->pv, 'en');
                    $upd_query = "UPDATE B1_phrasal_verbs SET conceptnet_canonical='" . $upd_cform . "' WHERE id=" . $row->id;
                    $rs1 = $db->Execute($upd_query);
                }
                break;
            }
        case 'get_synonyms': {
                $query = "SELECT id,conceptnet_canonical FROM B1_phrasal_verbs where id>0";
                $rs = $db->Execute($query);
                while ($row = $rs->FetchNextObject(false)) {
                    $upd_cform = get_conceptnet_objects_all_for("Synonym", $row->conceptnet_canonical, 1000, "edges");
                    $upd_query = "UPDATE B1_phrasal_verbs SET synonyms='" . $upd_cform . "' WHERE id=" . $row->id;
                    $rs1 = $db->Execute($upd_query);
                }

                break;
            }
        case 'get_sentences': {
                $query = "SELECT id,conceptnet_canonical FROM B1_phrasal_verbs where id>0";
                $rs = $db->Execute($query);
                while ($row = $rs->FetchNextObject(false)) {
                    $upd_cform = get_conceptnet_objects_all_for("Synonym", $row->conceptnet_canonical, 1000, "sentences");
                    $upd_query = "UPDATE B1_phrasal_verbs SET sentences='" . $upd_cform . "' WHERE id=" . $row->id;
                    $rs1 = $db->Execute($upd_query);
                }

                break;
            }
    }
}

function filter_cambridge_dataset() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word,filter_object,objects_not_related FROM tbl_cambridge where id>0";
    //First we removed all B2
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject(false)) {
        $current_word = $row->word;
        //Remove all words that have a space or a dash
        $check_multiwords = (strpos($current_word, ' ') !== false) || (strpos($current_word, '-') !== false) || (strpos($current_word, '_') !== false);
        //Remove all exercises that have less than 10 related to terms
        $relatedto = explode("|", $row->filter_object);
        $relatedto_num = count($relatedto);
        //Remove all exercises that have less than 10 related to terms
        $notrelatedto = explode("|", $row->objects_not_related);
        $notrelatedto_num = count($notrelatedto);
        echo $check_multiwords . "|" . $relatedto_num . "|" . $notrelatedto_num . "<br />";
        if ($check_multiwords || $relatedto_num < 10 || $notrelatedto_num < 10) {
            echo "Deleting id=" . $row->id . "<br />";
            $query2 = "DELETE FROM tbl_cambridge where id=" . $row->id;
            $rs2 = $db->Execute($query2);
        }
    }
}

function cambridge_check_conceptnet() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word FROM tbl_cambridge where id>0";
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $conceptnet = conceptnet_search_for_proper_uri($row->WORD, 'en');
        $query2 = "UPDATE tbl_cambridge SET conceptnet='" . $conceptnet . "' where id=" . $row->ID;
        $rs2 = $db->Execute($query2);
    }
}

function choose_exercise($userid = "", $lang = "en") {
    //$req_close = 20; //Original value for experiment
    $req_close = 50;
    //$req_open = 80; //Original value for experiment
    $req_open = 50;
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT type FROM tbl_exercise_log where uid='" . $userid . "' and lang='".$lang."'";
    $rs = $db->Execute($query);
    $open = 0;
    $close = 0;
    while ($row = $rs->FetchNextObject()) {
        if ($row->TYPE === 'open') {
            $open++;
        } else {
            $close++;
        }
    }
    $total_user = $open + $close;
    $request = "";
    //If everything is empty start randomly
    if ($total_user === 0) {
        $typos = array('open', 'close');

        $k = array_rand($typos);
        $v = $typos[$k];
        $request = $v;
    } else {
        $per_open = round(($open * 100) / $total_user, 0);
        $per_close = round(($close * 100) / $total_user, 0);
        if ($per_open >= $req_open) {
            $request = 'close';
        } else {
            $request = 'open';
        }
    }
    $res_srv = new stdClass();
    $res_srv->exercise_type = $request;
    $res_srv->lang = $lang;
    $res_srv->limit_for_close_type = $req_close;
    $res_srv->limit_for_open_type = $req_open;
    $res_srv->perc_for_close_type = $per_open;
    $res_srv->perc_for_open_type = $per_close;
    header('content-type: application/json; charset=utf-8');
    echo json_encode($res_srv);
}

function analyze_conceptnet_results() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,subject,add_object FROM tbl_exercises where id>0 and add_object<>''";
    $rs = $db->Execute($query);

    $count = 0;
    $all_words = array();
    $full_res = array();
    //http://api.conceptnet.io/query?start=/c/en/cat&end=/c/en/fish&limit=1000
    while ($row = $rs->FetchNextObject()) {
        $mid_res = new stdClass();
        $mid_res->eid = $row->ID;
        //echo "analyzing exercise " . $row->ID . "<br>";
        //Get the number of new words
        $nwords1 = explode("|", $row->ADD_OBJECT);
        $nwords = array_unique($nwords1);
        $nwords = array_filter($nwords);
        //echo "Number of new words:".count($nwords)."<br>";
        $mid_res->unique_words = implode(",", $nwords);
        $mid_res->num_words = count($nwords1);
        $mid_res->num_unique_words = count($nwords);
        $relationsa = array();

        $count_words_rels = 0;

        foreach ($nwords as $nword) {
            $relations_relatedto = 0;
            $rels_ins = array();
            $all_words[] = $nword;

            //http://api.conceptnet.io/query?node=/c/en/dog&other=/c/en/bark
            $c_api = "http://api.conceptnet.io/query?node=/c/en/" . mb_strtolower($row->SUBJECT, 'UTF-8') . "&other=/c/en/" . mb_strtolower($nword, 'UTF-8') . "&limit=1000";
            $data = json_decode(file_get_contents($c_api));
            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {
                        if ($edge->rel->label !== "") {
                            if (($edge->rel->label !== "RelatedTo")) {
                                $rels_ins[] = $edge->rel->label;
                            }
                        } else {
                            //Check for relatedTo relations as a safeguard
                            $relations_relatedto++;
                        }
                    }
                }
            }
            $nres1 = ($rels_ins);
            $nres1 = array_filter($nres1);
            if (count($nres1) > 0) {
                $count_words_rels++;
            }
        }
        $mid_res->num_related_to = $relations_relatedto;
        $mid_res->num_words_relations = $count_words_rels;
        $mid_res->num_words_no_relations = ($mid_res->num_unique_words) - ($count_words_rels);

        //echo "<pre>";
        //print_r($relationsa);
        //echo "</pre>";

        $mid_res->num_new_relations = $count_words_rels;
        $mid_res->new_relations = implode(",", $nres1);

        $full_res[] = $mid_res;
        //sleep(1);
    }

    echo "<table>";
    echo "<tr><td>Eid</td><td>#RelatedTo</td><td>#Unique Words</td><td>Unique Words</td><td>Words without other Relations in ConceptNet</td><td>Words without other Relations in ConceptNet (%)</td><td>Words with new relations</td><td># New relations</td><td>New relations</td></tr>";
    $aunique_words = array();
    $awords_no_relations = array();
    $awords_new_relations = array();
    foreach ($full_res as $r) {
        $aunique_words[] = $r->num_unique_words;
        $awords_no_relations[] = $r->num_words_no_relations;
        $awords_new_relations[] = $r->num_new_relations;

        $number_of_words_no_relations_perc = number_format((float) (100 * ($r->num_words_no_relations) / ($r->num_unique_words)), 2, '.', '');
        echo "<tr><td>" . $r->eid . "</td><td>" . $r->num_related_to . "</td><td>" . $r->num_unique_words . "</td><td>" . $r->unique_words . "</td><td>" . $r->num_words_no_relations . "</td><td>" . $number_of_words_no_relations_perc . "%</td><td>" . $r->num_words_relations . "</td><td>" . $r->num_new_relations . "</td><td>" . $r->new_relations . "</td></tr>";
    }
    echo "</table>";
    $aunique_words = array_filter($aunique_words);
    $awords_no_relations = array_filter($awords_no_relations);
    $awords_new_relations = array_filter($awords_new_relations);
    echo "Total number of words: " . count($all_words) . "<br />";
    echo "Total number of unique words: " . count(array_unique($all_words)) . "<br />";
    echo "Average number of unique words (" . array_sum($aunique_words) . "/" . count(($aunique_words)) . "): " . array_sum($aunique_words) / count(($aunique_words)) . "<br />";
    echo "Average number of words without other relations (" . array_sum($awords_no_relations) . "/" . count(($awords_no_relations)) . "): " . array_sum($awords_no_relations) / count(($awords_no_relations)) . "<br />";
    echo "Average number of words with new relations (" . array_sum($awords_new_relations) . "/" . count(($awords_new_relations)) . "): " . array_sum($awords_new_relations) / count(($awords_new_relations)) . "<br />";
}

function analyze_closed_exercises_results() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,exercise_type,uid FROM tbl_exercises_close where id>0 and uid like 'telegram_%' and uid not in('telegram_130870321','telegram_701479656','telegram_159675857','telegram_373438098')";
    $rs = $db->Execute($query);
    $num_closed_exercises = 0;
    $num_closed_evaluated = 0;
    $num_closed_relatedto = 0;
    $num_closed_notrelatedto = 0;
    $users = array();
    while ($row = $rs->FetchNextObject(false)) {
        $users[] = $row->uid;
        $num_closed_exercises++;
        if ($row->exercise_type === '1') {
            $num_closed_evaluated++;
        }
        if ($row->exercise_type === '2') {
            $num_closed_relatedto++;
        }
        if ($row->exercise_type === '3') {
            $num_closed_notrelatedto++;
        }
    }
    echo "Unique number of users:" . count(array_unique($users)) . "<br />";
    echo "Total number of closed exercises:" . $num_closed_exercises . "<br />";
    echo "Total number of closed exercises (type 1):" . $num_closed_evaluated . "(" . ($num_closed_evaluated * 100) / $num_closed_exercises . "%)<br />";
    echo "Total number of closed exercises (type 2):" . $num_closed_relatedto . "(" . ($num_closed_relatedto * 100) / $num_closed_exercises . "%)<br />";
    echo "Total number of closed exercises (type 3):" . $num_closed_notrelatedto . "(" . ($num_closed_notrelatedto * 100) / $num_closed_exercises . "%)<br /><hr />";

    //analyze results
    $query1 = "SELECT a.id,a.eid,b.exercise_type,a.points FROM tbl_eval_close as a INNER JOIN tbl_exercises_close as b ON a.eid=b.id where a.id>0 and a.uid like 'telegram_%' and a.uid not in('telegram_130870321','telegram_701479656','telegram_159675857','telegram_373438098')";
    $rs1 = $db->Execute($query1);
    $num_closed_exercises_answered = 0;
    $num_closed_evaluated_answered = 0;
    $num_closed_relatedto_answered = 0;
    $num_closed_notrelatedto_answered = 0;
    $num_closed_answered_correctly = 0;
    $num_closed_evaluated_answered_correctly = 0;
    $num_closed_relatedto_answered_correctly = 0;
    $num_closed_notrelatedto_answered_correctly = 0;
    while ($row1 = $rs1->FetchNextObject(false)) {
        $num_closed_exercises_answered++;
        if ($row1->exercise_type === '1') {
            $num_closed_evaluated_answered++;
            if ($row1->points === '1') {
                $num_closed_answered_correctly++;
                $num_closed_evaluated_answered_correctly++;
            }
        }
        if ($row1->exercise_type === '2') {
            $num_closed_relatedto_answered++;
            if ($row1->points === '1') {
                $num_closed_answered_correctly++;
                $num_closed_relatedto_answered_correctly++;
            }
        }
        if ($row1->exercise_type === '3') {
            $num_closed_notrelatedto_answered++;
            if ($row1->points === '1') {
                $num_closed_answered_correctly++;
                $num_closed_notrelatedto_answered_correctly++;
            }
        }
    }
    echo "Total number of closed exercises answered:" . $num_closed_exercises_answered . "<br />";
    echo "Total number of closed exercises answered correctly:" . $num_closed_answered_correctly . "(" . ($num_closed_answered_correctly * 100) / $num_closed_exercises_answered . "%)<br />";

    echo "Total number of closed exercises answered (type 1):" . $num_closed_evaluated_answered . "(" . ($num_closed_evaluated_answered * 100) / $num_closed_exercises_answered . "%)<br />";
    echo "Total number of closed exercises answered correctly (type 1):" . $num_closed_evaluated_answered_correctly . "(" . ($num_closed_evaluated_answered_correctly * 100) / $num_closed_evaluated_answered . "%)<br />";

    echo "Total number of closed exercises answered (type 2):" . $num_closed_relatedto_answered . "(" . ($num_closed_relatedto_answered * 100) / $num_closed_exercises_answered . "%)<br />";
    echo "Total number of closed exercises answered correctly (type 2):" . $num_closed_relatedto_answered_correctly . "(" . ($num_closed_relatedto_answered_correctly * 100) / $num_closed_relatedto_answered . "%)<br />";

    echo "Total number of closed exercises answered (type 3):" . $num_closed_notrelatedto_answered . "(" . ($num_closed_notrelatedto_answered * 100) / $num_closed_exercises_answered . "%)<br />";
    echo "Total number of closed exercises answered correctly (type 3):" . $num_closed_notrelatedto_answered_correctly . "(" . ($num_closed_notrelatedto_answered_correctly * 100) / $num_closed_notrelatedto_answered . "%)<br />";

    $query2 = "SELECT id,uid,eid,response_hint,timestamp FROM tbl_responses_history";
    $rs2 = $db->Execute($query2);


    $same_words = array();
    $different_words = array();
    //http://api.conceptnet.io/query?start=/c/en/cat&end=/c/en/fish&limit=1000
    while ($row3 = $rs2->FetchNextObject(false)) {


        $query3 = "SELECT id,response FROM tbl_eval where uid='" . $row3->uid . "' and eid=" . $row3->eid . " and (timestamp>=" . $row3->timestamp . ") ORDER BY timestamp ASC LIMIT 1";
        $rs3 = $db->Execute($query3);
        while ($row4 = $rs3->FetchNextObject(false)) {
            if ($row4->response === $row3->response_hint) {
                $same_words[] = $row4->response;
            } else {
                $different_words[] = ($row3->response_hint . " -|- " . $row4->response);
            }
        }
    }
    echo "responses_same:" . count($same_words) . "<br/>";
    echo "responses_diffent:" . count($different_words) . "<br/>";
    echo "<pre>";
    print_r($different_words);
    echo "</pre>";
}

function reset_db($apikey = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $res = new stdClass();
    if ($apikey === "ssjdhfsd67f5sd76f5sd76fshfjsdfs7d") {
        $sql1 = "TRUNCATE table tbl_eval";
        $rs1 = $db->Execute($sql1);
        $sql2 = "TRUNCATE table tbl_users";
        $rs2 = $db->Execute($sql2);
        $sql3 = "UPDATE tbl_exercises SET add_object='' where id>0";
        $rs3 = $db->Execute($sql3);
        $sql4 = "TRUNCATE table tbl_exercises_close";
        $rs4 = $db->Execute($sql4);
        $sql5 = "TRUNCATE table tbl_exercise_log";
        $rs5 = $db->Execute($sql5);
        $sql6 = "TRUNCATE table tbl_responses_history";
        $rs6 = $db->Execute($sql6);
        $sql7 = "TRUNCATE table tbl_eval_close";
        $rs7 = $db->Execute($sql7);
        $res->success = true;
    } else {
        $res->success = false;
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($res);
}

function check_exercise_exists($eid = 0) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id FROM tbl_exercises where id=" . $eid;
    $rs = $db->Execute($query);

    $count = 0;
    while ($row = $rs->FetchNextObject()) {
        $count++;
    }
    if ($count > 0) {
        return true;
    } else {
        return false;
    }
}

function check_close_exercise_exists($eid = 0) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id FROM tbl_exercises_close where id=" . $eid;
    $rs = $db->Execute($query);

    $count = 0;
    while ($row = $rs->FetchNextObject()) {
        $count++;
    }
    if ($count > 0) {
        return true;
    } else {
        return false;
    }
}

function check_registered_user_exists($userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    //First check if the userid exists
    $query = "SELECT id,uid,first_name,timestamp FROM tbl_users where uid='" . $userid . "'";
    $rs = $db->Execute($query);

    $count = 0;
    while ($row = $rs->FetchNextObject()) {
        $count++;
    }
    if ($count > 0) {
        return true;
    } else {
        return false;
    }
}

function log_exercise($type = "open", $userid = 0, $eid = 0, $ctype_type = 0,$lang="en") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_exercise_log";
    $ins = array();

    $ins['type'] = $type;
    $ins['uid'] = $userid;
    $ins['eid'] = $eid;
    $ins['ctype_type'] = $ctype_type;
    $ins['timestamp'] = time();
    $ins['lang']=$lang;
    $db->Autoexecute($table, $ins, 'INSERT');
}

function get_exercise_languages() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $sql = "SELECT DISTINCT lang FROM tbl_exercises";


    $languages = array();
    $rs = $db->Execute($sql);
    while ($row = $rs->FetchNextObject()) {
        $language = new stdClass();
        $language->language_code = $row->LANG;
        $languages[] = $row->LANG;
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($languages);
}

function add_user($userid = 0, $first_name = "", $language_exercise = "en", $language_interface = "en") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_users";
    $ins = array();

    $ins['uid'] = ($userid);
    $ins['first_name'] = ($first_name);
    $ins['language_exercise'] = $language_exercise;
    $ins['language_interface'] = $language_interface;
    $ins['reliability'] = 0.5;



    $ins['timestamp'] = time();

    $res = new stdClass();
    if ($ins['first_name'] !== "" && $ins['uid'] !== "") {
        if (check_registered_user_exists($userid)) {
            $res->success = false;
            $res->error = "The userid already exists.";
        } else {
            if ($db->Autoexecute($table, $ins, 'INSERT')) {
                $res->success = true;
            } else {
                $res->success = false;
                $res->error = $db->error;
            }
        }
    } else {
        $res->success = false;
        $res->error = "Missing values. This method requires both the userid and the name of the user.";
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($res);
}

function update_user($userid = 0, $first_name = "", $language_exercise = "en", $language_interface = "en") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_users";

    $sql = "UPDATE tbl_users SET first_name='" . $first_name . "',language_exercise='" . $language_exercise . "',language_interface='" . $language_interface . "' WHERE uid='" . $userid . "'";
//echo $sql;
    //$ins['timestamp'] = time();

    $res = new stdClass();
    if ($ins['first_name'] !== "" && $ins['uid'] !== "") {
        if (check_registered_user_exists($userid)) {
            if ($db->Execute($sql)) {
                $res->success = true;
            } else {
                $res->success = false;
                $res->error = $db->error;
            }
        } else {
            $res->success = false;
            $res->error = "The userid does not exist.";
        }
    } else {
        $res->success = false;
        $res->error = "Missing values. This method requires both the userid and the name of the user.";
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($res);
}


function telegram_add_userdata($telegram_id,$telegram_userdata) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    #$db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "telegram_user_data";
    
    $query = "INSERT INTO ".$table." (`id`,`userdata`) VALUES ('" . $telegram_id . "','".addslashes($telegram_userdata)."')";
    echo $query;
    $rs = $db->Execute($query);
}

function telegram_record_userdata($telegram_id,$telegram_userdata) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    #$db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "telegram_user_data";
    
    $query = "UPDATE ".$table." set userdata='".addslashes($telegram_userdata)."' WHERE id='" . $telegram_id . "'";
    echo $query;
    $rs = $db->Execute($query);
}

function telegram_get_userdata($telegram_id) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->debug = $cfg->db_debug;
    $table = "telegram_user_data";

    $query = "SELECT userdata FROM ".$table." WHERE id='" . $telegram_id . "'";
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $res = $row->USERDATA;
    }
    
    if(isset($res)){
        header('content-type: application/json; charset=utf-8');
        echo $res;
    }else{
        echo "None";
    }
}

//Function to get back a random response for an exercise and a user
function get_random_response($eid = 0, $userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    //First get all the response from that user for that exercise
    $query = "SELECT id,uid,response FROM tbl_eval where uid='" . $userid . "' and eid=" . $eid;
    $rs = $db->Execute($query);
    $given_responses = array();

    while ($row = $rs->FetchNextObject()) {
        $given_responses[] = $row->RESPONSE;
    }
    $given_responses = array_filter($given_responses);
    //Then get all possible responses for that exercise
    $query = "SELECT id,add_object,filter_object FROM tbl_exercises where id='" . $eid . "'";

    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        //$countr++;
        $add_object = explode("|", $row->ADD_OBJECT);
        $filter_object = explode("|", $row->FILTER_OBJECT);
    }
    
    $add_object_lower = array();
    foreach ($add_object as $add_object_element) {
        $add_object_lower[] = mb_strtolower($add_object_element, 'UTF-8');
    }
    $filter_object_lower = array();
    foreach ($filter_object as $filter_object_element) {
        $filter_object_lower[] = mb_strtolower($filter_object_element, 'UTF-8');
    }
    $result = array_merge($add_object_lower, $filter_object_lower);
    //Clear the merged arrays
    $result = array_filter($result);
    //Remove the responses that the user already gave back
    $good_responses = array_diff($result, $given_responses);
    //Select a random value from the result array
    $rand_key = array_rand($good_responses);
    //echo $rand_keys;
    $random_value_string = $good_responses[$rand_key];
    //Make a nice json response
    $res = new stdClass();
    //$res->id = $row->ID;
    $res->eid = $eid;
    $res->userid = $userid;
    $res->response = $random_value_string;
    //Store that in the database
    $ins = array();
    $ins['eid'] = $res->eid;
    $ins['uid'] = $res->userid;
    $ins['response_hint'] = $res->response;
    $ins['timestamp'] = time();
    $db->Autoexecute("tbl_responses_history", $ins, 'INSERT');
    header('content-type: application/json; charset=utf-8');
    echo json_encode($res);
}

function get_user_info($userid = 0, $request_type = "internal") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,uid,first_name,language_interface,language_exercise,timestamp FROM tbl_users where uid='" . $userid . "'";
    $rs = $db->Execute($query);
    $reso = array();
    $prev_points = "-1";
    if ($userid !== "") {
        while ($row = $rs->FetchNextObject()) {
            $res = new stdClass();
            //$res->id = $row->ID;
            $res->userid = $row->UID;
            $res->uname = $row->FIRST_NAME;
            $res->language_interface = $row->LANGUAGE_INTERFACE;
            $res->language_exercise = $row->LANGUAGE_EXERCISE;
            $res->timestamp = $row->TIMESTAMP;
            $prev_points = $res->earned_points;
            $reso[] = $res;
        }
    }
    if ($request_type === "internal") {
        return $reso;
    } else {
        if ($userid === "") {
            $res = new stdClass();
            $res->success = false;
            $res->error = "This method requires a userid.";
        }
        header('content-type: application/json; charset=utf-8');
        echo json_encode($reso);
    }
}

function get_leaderboard() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT uid,first_name FROM tbl_users";
    $rs = $db->Execute($query);
    $reso = array();
    $reso_final = array();

    $prev_rank = 0;
    while ($row = $rs->FetchNextObject()) {
        $res = new stdClass();
        $res->userid = $row->UID;
        //get urerinfo
        $userinfo = get_user_info($res->userid, "internal");
        //Get the score for the closed exercises
        $score_close = get_points_close($row->UID);
        $res->userid = $row->UID;
        $res->uname = $userinfo[0]->uname;
        $res->earned_points = intval(intval(retrieve_points_user($res->userid, "earned")) + intval($score_close));
        $res->potential_points = intval(retrieve_points_user($res->userid, "potential"));
        $res->badges = intval(retrieve_badges_user($row->UID));
        //Add a rank of zero initially
        $res->rank = 0;
        if ($res->earned_points === 0) {
            
        } else {
            $reso[] = $res;
        }
    }
    usort($reso, "cmp_earned_points");
    $prev_points = "-1";
    foreach ($reso as $reso_obj) {
        if ($prev_points === $reso_obj->earned_points) {
            $reso_obj->rank = $prev_rank;
        } else {
            $prev_rank++;
            $reso_obj->rank = $prev_rank;
        }
        $prev_points = $reso_obj->earned_points;
        $reso_final[] = $reso_obj;
    }

    header('content-type: application/json; charset=utf-8');
    echo json_encode($reso_final);
}

function cmp_earned_points($a, $b) {
    return $b->earned_points >= $a->earned_points;
}

function retrieve_points_user($userid = "", $type = "earned") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    if ($type === "earned") {
        $query = "SELECT SUM(points) as suma,uid FROM tbl_eval where NOT(points IS NULL) and uid='" . $userid . "' GROUP BY uid";
    } else {
        $query = "SELECT COUNT(id) as suma,uid FROM tbl_eval where points IS NULL and uid='" . $userid . "' GROUP BY uid";
    }
    $rs = $db->Execute($query);
    $res = 0;
    while ($row = $rs->FetchNextObject()) {
        $res = $row->SUMA;
    }
    if ($type === "earned") {
        return intval($res);
    } else {
        return $res * 2;
    }
}

function give_badge($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $q1 = "SELECT id,uid FROM tbl_eval where (points IS NULL) and eid='" . $eid . "' and LOWER(response) like '" . mb_strtolower($response, 'UTF-8') . "' ORDER BY timestamp ASC LIMIT 1";
    $rs = $db->Execute($q1);
    $res = 0;
    while ($row = $rs->FetchNextObject()) {
        $res = $row->ID;
    }

    $query = "UPDATE tbl_eval SET badge=1,tobe_notified=1 where id='" . $res . "'";
    $rs = $db->Execute($query);
}

function retrieve_badges_user($userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT SUM(badge) as suma,uid FROM tbl_eval where NOT(points IS NULL) and uid='" . $userid . "' GROUP BY uid";

    $rs = $db->Execute($query);
    $res = 0;
    while ($row = $rs->FetchNextObject()) {
        $res = $row->SUMA;
    }
    return intval($res);
}

function response_exist($userid = "", $eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT * FROM tbl_eval where uid='" . $userid . "' and eid=" . $eid . " and LOWER(response) COLLATE UTF8_GENERAL_CI like '" . mb_strtolower($response, 'UTF-8') . "'";
    $countr = 0;
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $countr++;
    }

    if ($countr === 0) {
        return false;
    } else {
        return true;
    }
}

function response_exist_list_objects($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    //$query = "SELECT * FROM tbl_exercises where id='" . $eid . "' and ((LOWER(filter_object) like '%" . strtolower($response) . "%') OR (LOWER(add_object) like '%" . strtolower($response) . "%'))";

    $query = "SELECT id,add_object,filter_object FROM tbl_exercises where id='" . $eid . "'";
    $countr = 0;
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        //$countr++;
        $add_object = explode("|", $row->ADD_OBJECT);
        $filter_object = explode("|", $row->FILTER_OBJECT);
    }

    //Perform a case incensitive search
    $add_object_lower = array();
    foreach ($add_object as $add_object_element) {
        $add_object_lower[] = mb_strtolower($add_object_element, 'UTF-8');
    }
    $filter_object_lower = array();
    foreach ($filter_object as $filter_object_element) {
        $filter_object_lower[] = mb_strtolower($filter_object_element, 'UTF-8');
    }
    $exist_response_add_object = in_array(mb_strtolower($response, 'UTF-8'), $add_object_lower);
    $exist_response_filter_object = in_array(mb_strtolower($response, 'UTF-8'), $filter_object_lower);

    if (!$exist_response_add_object && !$exist_response_filter_object) {
        return false;
    } else {
        return true;
    }
}

function count_null_responses_for_exercise($eid = 0) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "SELECT * FROM tbl_eval where eid='" . $eid . "' and (points IS NULL)";
    $countr = 0;
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $countr++;
    }
    return $countr;
}

function count_responses_for_exercise_for_response($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "SELECT count(id) as num, response FROM tbl_eval where eid='" . $eid . "' and points IS NULL GROUP BY response ORDER BY num DESC LIMIT 1";
    $countr = 0;
    $num = 0;
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $num = $row->NUM;
    }
    return intval($num);
}

function check_response_is_top_ranked($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "SELECT count(id) as num, response FROM tbl_eval where eid='" . $eid . "' and points IS NULL GROUP BY response ORDER BY num DESC LIMIT 1";
    $max_r = "";
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $max_r = $row->NUM;
    }
    $query = "SELECT count(id) as num, response FROM tbl_eval where eid='" . $eid . "' and points IS NULL and LOWER(response) like '%" . mb_strtolower($response, 'UTF-8') . "%' GROUP BY response having num=" . $max_r . " ORDER BY num DESC";
    $rs1 = $db->Execute($query);
    $countr = 0;
    while ($row1 = $rs1->FetchNextObject()) {
        $countr++;
    }
    if ($countr === 0) {
        return false;
    } else {
        return true;
    }
}

function retrieve_top_ranked_response($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "SELECT count(id) as num, response FROM tbl_eval where eid='" . $eid . "' and points IS NULL GROUP BY response ORDER BY num DESC LIMIT 1";
    $max_r = "";
    $response = "";
    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $max_r = $row->NUM;
        $response = $row->RESPONSE;
    }

    $res = new stdClass();
    $res->response = $response;
    $res->num = $max_r;
    return $res;
}

function update_eval_responses($eid = 0, $response = "", $points = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "UPDATE tbl_eval SET points=" . $points . ",notified=0,tobe_notified=1 where (points IS NULL) and LOWER(response) like '" . mb_strtolower($response, 'UTF-8') . "' and eid='" . $eid . "'";

    $rs = $db->Execute($query);
    return true;
}

function update_exercise_responses($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    //First get the database entry for the exercise
    $query = "SELECT id,add_object FROM tbl_exercises where id='" . $eid . "'";

    $rs = $db->Execute($query);
    while ($row = $rs->FetchNextObject()) {
        $add_object = explode("|", $row->ADD_OBJECT);
        $add_object_filtered = array_filter($add_object);
    }
    //Add the new response to the list
    $add_object_filtered[] = trim($response);
    $add_object_string = implode("|", $add_object_filtered);

    //Update the database field for the exercise
    $query = "UPDATE tbl_exercises SET add_object='" . $add_object_string . "' where id='" . $eid . "'";

    $rs = $db->Execute($query);
    return true;
}

function update_answers($eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "UPDATE tbl_eval SET points=" . $points . " where points IS NULL and LOWER(response) like '" . mb_strtolower($response, 'UTF-8') . "' and eid='" . $eid . "'";

    $rs = $db->Execute($query);
    return true;
}

function retrieve_points($userid = "", $eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $points = "";
    //Check that the user did not respond with the same reponse for that exercise
    //IF yes points=0
    if (response_exist($userid, $eid, $response)) {
        $points = "0";
    } else {
        //Check if the response is included in the object set
        //IF YES points=1
        if (response_exist_list_objects($eid, $response)) {
            $points = "1";
        } else {
            //IF No
            //CHECK that we have at least N null reponses for that exercise
            //IF NO points=null
            $count_nulls = count_null_responses_for_exercise($eid);
            if ($count_nulls < 5) {
                $points = "";
            } else {
                //IF YES count the occurences of response for that exercise that are null
                if (count_responses_for_exercise_for_response($eid) < 2) {
                    $points = "";
                } else {

                    $res_obj = retrieve_top_ranked_response($eid);
                    $nresponse = $res_obj->response;
                    if ($res_obj->num < 2) {
                        $points = "";
                    } else {
                        give_badge($eid, $nresponse);
                        update_eval_responses($eid, $nresponse, 2);
                        update_exercise_responses($eid, $nresponse);

                        $points = "";
                    }
                }
            }
        }
    }
    return $points;
}

function is_duplicate_response($userid = 0, $eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $query = "SELECT id FROM tbl_eval where (uid='" . $userid . "' AND eid='" . $eid . "' AND response='" . $response . "')";

    $rs = $db->Execute($query);
    $i = 0;
    while ($row = $rs->FetchNextObject()) {
        $i++;
    }
    if ($i > 0) {
        return true;
    } else {
        return false;
    }
}

function store_response($userid = 0, $eid = 0, $response = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_eval";
    $ins = array();
    //Make some checks first
    $errors = array();
    if (($userid === "") || ($eid === "")) {
        $errors[] = "Both the userid and the exerciseid must be populated.";
    }
    if ($userid !== "") {
        if (!check_registered_user_exists($userid)) {
            $errors[] = "No user was found with userid=" . $userid . ".";
        }
    }
    if ($eid !== "") {
        if (!check_exercise_exists($eid)) {
            $errors[] = "No exercise was found with eid=" . $eid . ".";
        }
    }

    //Make sure that the same user did not give the same response for the same exercise
    //if (is_duplicate_response($userid, $eid, $response)) {
    //    $errors[] = "Duplicate response.";
    //}
    //Now prepare the response
    $response_o = new stdClass();
    if (count($errors) === 0) {
        $response_o->eid = intval($eid);
        $response_o->userid = $userid;
        $response_o->points = retrieve_points($userid, $eid, $response);
        $response_o->response = mb_strtolower($response, 'UTF-8');

        $ins['uid'] = $userid;
        $ins['eid'] = $eid;
        $ins['response'] = mb_strtolower($response, 'UTF-8');
        ;
        if ($response_o->points === "") {
            $p = null;
            $response_o->points = null;
            $ins['notified'] = 0;
            $ins['tobe_notified'] = 0;
        } else {
            $p = intval($response_o->points);
            $response_o->points = intval($response_o->points);
            $ins['notified'] = 1;
            $ins['tobe_notified'] = 0;
        }
        $ins['points'] = $p;
        $ins['timestamp'] = time();
        $db->Autoexecute($table, $ins, 'INSERT');
    } else {
        $response_o->success = false;
        $response_o->error = implode(";", $errors);
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($response_o);
}

function get_close_exercise_info($eid = 0) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_exercises_close";
    $query = "SELECT id,eid_originated,exercise_type FROM " . $table . " where id=" . $eid;

    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject(false)) {
        $info = new stdClass();
        $info->id = $row->id;
        $info->eid_originated = $row->eid_originated;
        $info->exercise_type = $row->exercise_type;
    }
    return $info;
}

function store_close_response($userid = 0, $eid = 0, $response = 0) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_eval_close";
    $ins = array();
    //Make some checks first
    $errors = array();
    if (($userid === "") || ($eid === "")) {
        $errors[] = "Both the userid and the exerciseid must be populated.";
    }
    if ($userid !== "") {
        if (!check_registered_user_exists($userid)) {
            $errors[] = "No user was found with userid=" . $userid . ".";
        }
    }
    if ($eid !== "") {
        if (!check_close_exercise_exists($eid)) {
            $errors[] = "No closed exercise was found with eid=" . $eid . ".";
        }
    }

    //Make sure that the same user did not give the same response for the same exercise
    //if (is_duplicate_response($userid, $eid, $response)) {
    //    $errors[] = "Duplicate response.";
    //}
    //Now prepare the response
    $info = get_close_exercise_info($eid);
    $response = intval($response);
    $correct_response = -1;
    switch ($info->exercise_type) {
        case 1: {
                $correct_response = 1;
                if ($response === 1) {
                    $p = 1;
                } else {
                    $p = 0;
                }
                break;
            }
        case 2: {
                $correct_response = 1;
                if ($response === 1) {
                    $p = 1;
                } else {
                    $p = 0;
                }
                break;
            }
        case 3: {
                $correct_response = -1;
                if ($response === -1) {
                    $p = 1;
                } else {
                    $p = 0;
                }
                break;
            }
    }

    $response_o = new stdClass();
    if (count($errors) === 0) {
        $response_o->eid = intval($eid);
        $response_o->userid = $userid;
        //$response_o->points = retrieve_points($userid, $eid, $response);
        $response_o->points = $p;

        $response_o->response = intval($response);
        $response_o->correct_response = intval($correct_response);

        $ins['uid'] = $userid;
        $ins['eid'] = $eid;
        $ins['response'] = intval($response);

        $ins['points'] = $p;
        $ins['timestamp'] = time();
        $db->Autoexecute($table, $ins, 'INSERT');
    } else {
        $response_o->success = false;
        $response_o->error = implode(";", $errors);
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($response_o);
}

function get_points_close($userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_eval_close";
    $query = "SELECT DISTINCT a.uid,SUM(a.points) as score FROM " . $table . " as a where uid='" . $userid . "'";
    $rs = $db->Execute($query);
    $score = 0;
    while ($row = $rs->FetchNextObject(false)) {
        $score = intval($row->score);
    }
    return $score;
}

function get_points($userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_eval";
    //Now prepare the response
    $response_o = new stdClass();
    if ($userid !== "") {
        $summary = new stdClass();
        $score_close = get_points_close($userid);
        $summary->earned_points = retrieve_points_user($userid, "earned") + $score_close;
        $summary->potential_points = retrieve_points_user($userid, "potential");
        $summary->badges = retrieve_badges_user($userid);

        $query = "SELECT DISTINCT a.id,a.uid,a.eid,a.response,a.points,a.badge,a.notified,a.timestamp FROM " . $table . " as a where uid='" . $userid . "'";
        $rs = $db->Execute($query);
        $arr = array();

        while ($row = $rs->FetchNextObject()) {
            $data_o = new stdClass();
            $data_o->id = $row->ID;
            $data_o->eid = intval($row->EID);
            $data_o->userid = $row->UID;
            $data_o->response = $row->RESPONSE;
            $data_o->points = intval($row->POINTS);
            $data_o->badge = intval($row->BADGE);
            $data_o->notified = intval($row->NOTIFIED);
            $data_o->timestamp = $row->TIMESTAMP;
            $arr[] = $data_o;
        }

        $response_o->summary = $summary;
        $response_o->responses = $arr;
    } else {
        $response_o->success = false;
        $response_o->error = "This method requires a userid.";
    }

    header('content-type: application/json; charset=utf-8');
    echo json_encode($response_o);
}

function get_notifications($userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_eval";
    $response_o = new stdClass();

    if ($userid !== "") {
        $query = "SELECT DISTINCT a.id,a.uid,a.eid,a.response,a.points,a.badge,a.timestamp,a.notified FROM " . $table . " as a where uid='" . $userid . "' and notified=0 and tobe_notified=1";
        $rs = $db->Execute($query);
        $arr = array();
        while ($row = $rs->FetchNextObject()) {
            $exercise_properties = get_single_exercise($row->EID);
            $data_o = new stdClass();
            //$data_o->id = $row->ID;
            $data_o->eid = intval($row->EID);
            $data_o->userid = $row->UID;
            $data_o->exercise = $exercise_properties->exercise;
            $data_o->response = $row->RESPONSE;
            $data_o->points = intval($row->POINTS);
            $data_o->badge = intval($row->BADGE);
            $data_o->timestamp = $row->TIMESTAMP;
            $arr[] = $data_o;
        }
        $query = "UPDATE " . $table . " SET notified=1,tobe_notified=0 where NOT(points IS NULL) and uid='" . $userid . "' and notified=0";
        $rs = $db->Execute($query);
        //Now prepare the response

        $response_o->responses = $arr;
    } else {
        $response_o->success = false;
        $response_o->error = "This method requires a userid.";
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($response_o);
}

function get_user_responses_for_exercise($eid = 0, $userid = "") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT DISTINCT a.response FROM tbl_eval as a where uid='" . $userid . "' and eid=" . $eid;
    $rs = $db->Execute($query);
    $arr = array();
    while ($row = $rs->FetchNextObject()) {
        $arr[] = $row->RESPONSE;
    }
    return $arr;
}

function check_close_exercises_exist() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_exercises_close";
    $query = "SELECT DISTINCT a.id FROM tbl_exercises as a WHERE (id>0 AND add_object<>'')";
    $rs = $db->Execute($query);
    $arr = 0;
    while ($row = $rs->FetchNextObject()) {
        $arr++;
    }
    if ($arr > 5) {
        return true;
    } else {
        return false;
    }
}

function get_close_exercise_type($userid = "") {
    $type1_per = 0;
    $type2_per = 50;
    $type3_per = 50;
    $type_arr = array();

    if (check_close_exercises_exist()) {
        for ($i = 0; $i < $type1_per; $i++) {
            $type_arr[] = 1;
        }
        for ($j = 0; $j < $type2_per; $j++) {
            $type_arr[] = 2;
        }
        for ($m = 0; $m < $type3_per; $m++) {
            $type_arr[] = 3;
        }
    } else {
        for ($i = 0; $i < ($type2_per); $i++) {
            $type_arr[] = 2;
        }
        for ($j = 0; $j < ($type3_per); $j++) {
            $type_arr[] = 3;
        }
    }
    shuffle($type_arr);
    $k = array_rand($type_arr);
    $v = $type_arr[$k];
    $request = $v;
    /* $cfg = new sc_config();
      $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
      $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
      $db->execute("set names 'utf8'");
      $db->debug = $cfg->db_debug;
      $query = "SELECT ctype_type FROM tbl_exercise_log where type='close' and uid='" . $userid . "'";
      $rs = $db->Execute($query);
      $type1 = 0;
      $type2 = 0;
      $type3 = 0;
      while ($row = $rs->FetchNextObject(false)) {
      switch ($row->ctype_type) {
      case 1: {
      $type1++;
      break;
      }
      case 2: {
      $type2++;
      break;
      }
      case 3: {
      $type3++;
      break;
      }
      }
      }
      $total_user = $type1 + $type2 + $type3;
      $request = "";
      //If everything is empty start randomly
      if ($total_user === 0) {
      $typos = array(1, 2, 3);

      $k = array_rand($typos);
      $v = $typos[$k];
      $request = $v;
      } else {
      $per_type1 = round(($type1_per * 100) / $total_user, 0);
      $per_type2 = round(($type2_per * 100) / $total_user, 0);
      $per_type3 = round(($type3_per * 100) / $total_user, 0);
      if ($per_type1 >= ($per_type2 + $per_type3)) {
      if ($per_type3 >= $per_type2) {
      $request = 2;
      } else {
      $request = 3;
      }
      } elseif ($per_type2 >= ($per_type1 + $per_type3)) {
      if ($per_type1 >= $per_type3) {
      $request = 3;
      } else {
      if (check_close_exercises_exist()) {
      $request = 1;
      } else {
      $request = 2;
      }
      }
      } elseif ($per_type3 >= ($per_type1 + $per_type2)) {
      if ($per_type1 >= $per_type2) {
      $request = 2;
      } else {
      if (check_close_exercises_exist()) {
      $request = 1;
      } else {
      $request = 3;
      }
      }
      } else {
      $typos = array(2, 3);

      $k = array_rand($typos);
      $v = $typos[$k];
      $request = $v;
      }
      }
     * */

    return $request;
}

function get_close_exercise($userid = "", $elevel = "", $etype = "RelatedTo", $lang = 'en') {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;
    $table = "tbl_exercises_close";
    $ins = array();
    //FIrst test the exercise type
    $close_type = get_close_exercise_type($userid);

    $ctype = -1;
    if ($elevel !== "") {
        $condition_level = " AND level='" . $elevel . "'";
    } else {
        $condition_level = "";
    }
    switch ($close_type) {
        case 1: {
                //Get values from the add_object (already verified)
                $select_clause = "a.add_object as conceptnet_objects";
                $secondary_clause = "AND add_object<>''";
                $ctype = 1;
                break;
            }
        case 2: {
                //Get values from the related_to objects (not verified)
                $select_clause = "a.filter_object as conceptnet_objects";
                $secondary_clause = "AND filter_object<>''";
                $ctype = 2;
                break;
            }
        case 3: {
                //Get values from the objects_not_related (not verified)
                $select_clause = "a.objects_not_related as conceptnet_objects";
                $secondary_clause = "AND objects_not_related<>''";
                $ctype = 3;
                break;
            }
        default: {
                //Get values from the related_to objects (not verified)
                $select_clause = "a.filter_object as conceptnet_objects";
                $secondary_clause = "AND filter_object<>''";
                $ctype = 2;
            }
    }
    if ($userid !== "") {
        if ($etype === "") {
            $query = "SELECT DISTINCT a.id,a.level,a.category,a.relation,a.subject," . $select_clause . ",a.lang FROM tbl_exercises as a WHERE (id>0 " . $secondary_clause . " AND lang='" . $lang . "'" . $condition_level . ") ORDER BY RAND() LIMIT 1";
        } else {
            $query = "SELECT DISTINCT a.id,a.level,a.category,a.relation,a.subject," . $select_clause . ",a.lang FROM tbl_exercises as a where (id>0 " . $secondary_clause . " AND relation='" . $etype . "' and lang='" . $lang . "'" . $condition_level . ") ORDER BY RAND() LIMIT 1";
        }
//echo $query;
        //echo $query;
        $rs = $db->Execute($query);
        $arr = array();
        $counter = 0;
        while ($row = $rs->FetchNextObject()) {
            $counter++;
            $data_o = new stdClass();
            $data_o->eid_originated = intval($row->ID);
            $data_o->userid = $userid;
            $data_o->close_type = $ctype;
            //Break the various possible answers
            $ans_i = explode("|", $row->CONCEPTNET_OBJECTS);
            $randIndex = array_rand($ans_i);
            $random_object = $ans_i[$randIndex];
            //Prepare the exercise string
            switch ($lang) {
                case 'en': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is related to " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is located at " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is part of " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'pt': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "É verdade que " . $row->SUBJECT . " está relacionado com " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "É verdade que " . $row->SUBJECT . " está localizado em " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "É verdade que " . $row->SUBJECT . " é parte de " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'sh': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Da li je tačno da su " . $row->SUBJECT . " i " . $random_object . " međusobno povezani?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Da li tačno da je " . $row->SUBJECT . " deo " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Da li je tačno da se " . $row->SUBJECT . " nalazi na/u " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'ro': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Este adevărat că " . $row->SUBJECT . " este legat de " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Este adevărat că " . $row->SUBJECT . " este situat la " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Este adevărat că " . $row->SUBJECT . " face parte din " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'el': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Ειναι ορθό ότι το " . $row->SUBJECT . " σχετίζεται με " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Ειναι ορθό ότι το " . $row->SUBJECT . " βρίσκεται στο " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Ειναι ορθό ότι το " . $row->SUBJECT . " είναι μέρος του " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'fr': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " et " . $random_object . " sont liés ?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " se situe en/au " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " fait partie de " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'it': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "È vero che " . $row->SUBJECT . " è un termine connesso al concetto di " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "È vero che " . $row->SUBJECT . " è localizzato in/a " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "È vero che " . $row->SUBJECT . " è una parte di  " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'fi': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " et " . $random_object . " sont liés ?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " se situe en/au " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " fait partie de " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'ru': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " et " . $random_object . " sont liés ?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " se situe en/au " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Est ce correct de dire que " . $row->SUBJECT . " fait partie de " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                case 'de': {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Stimmt es, dass " . $row->SUBJECT . " in Beziehung zu " . $random_object . " steht?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Stimmt es, dass " . $row->SUBJECT . " sich auf/in " . $random_object . " befindet?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Stimmt es, dass " . $row->SUBJECT . " Teil von " . $random_object . " ist??";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
                default: {
                        if ($row->RELATION === 'RelatedTo') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is related to " . $random_object . "?";
                        } elseif ($row->RELATION === 'AtLocation') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is located at " . $random_object . "?";
                        } elseif ($row->RELATION === 'PartOf') {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " is part of " . $random_object . "?";
                        } else {
                            $data_o->exercise = "Is it true that " . $row->SUBJECT . " " . $row->RELATION . " " . $random_object . "?";
                        }

                        break;
                    }
            }

            $data_o->relation = $row->RELATION;
            $data_o->subject = $row->SUBJECT;
            //$data_o->previous_responses = get_user_responses_for_exercise($data_o->eid, $userid);
            $data_o->object = $random_object;
            $data_o->level = $row->LEVEL;
            $data_o->category = $row->CATEGORY;
            $data_o->language = $row->LANG;

            //Record this in the database
            $ins['uid'] = $userid;
            $ins['level'] = $data_o->level;
            $ins['category'] = $data_o->category;
            $ins['exercise'] = $data_o->exercise;
            $ins['relation'] = $data_o->relation;
            $ins['subject'] = $data_o->subject;
            $ins['object'] = $data_o->object;
            $ins['created_timestamp'] = time();
            $ins['lang'] = $row->LANG;
            $ins['exercise_type'] = $ctype;

            $ins['truth_level'] = 'silver';
            $ins['reliabiity'] = 0.5;

            $ins['eid_originated'] = $data_o->eid_originated;
            $db->Autoexecute($table, $ins, 'INSERT');
            $data_o->eid = intval($db->insert_Id());
            $arr[] = $data_o;
            log_exercise('close', $userid, $data_o->eid, $ctype,$lang);
        }
    } else {
        $data_o = new stdClass();
        $data_o->success = false;
        $data_o->error = "This method requires a userid.";
    }
    if ($counter === 0) {
        $data_o = new stdClass();
        $data_o->success = false;
        $data_o->error = "No exercises were generated.";
    }
    header('content-type: application/json; charset=utf-8');
    //header("access-control-allow-origin: *");
    //header('Access-Control-Allow-Origin: *');
    echo json_encode($data_o);
}

function get_exercise($userid = "", $elevel = "", $etype = "RelatedTo", $lang = "en") {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    if ($elevel !== "") {
        $condition_level = " AND level='" . $elevel . "'";
    } else {
        $condition_level = "";
    }
    if ($userid !== "") {
        if ($etype === "") {
            $query = "SELECT DISTINCT a.id,a.level,a.category,a.exercise,a.relation,a.subject,a.object,a.lang FROM tbl_exercises as a WHERE (id>0 AND lang='" . $lang . "'" . $condition_level . ") ORDER BY RAND() LIMIT 1";
        } else {
            $query = "SELECT DISTINCT a.id,a.level,a.category,a.exercise,a.relation,a.subject,a.object,a.lang FROM tbl_exercises as a where (id>0 AND relation='" . $etype . "' AND lang='" . $lang . "'" . $condition_level . ") ORDER BY RAND() LIMIT 1";
        }

        //echo $query;
        $rs = $db->Execute($query);
        $arr = array();
        while ($row = $rs->FetchNextObject()) {
            $data_o = new stdClass();
            $data_o->eid = intval($row->ID);
            $data_o->userid = $userid;
            $data_o->exercise = $row->EXERCISE;
            $data_o->relation = $row->RELATION;
            $data_o->subject = $row->SUBJECT;
            $data_o->previous_responses = get_user_responses_for_exercise($data_o->eid, $userid);
            $data_o->object = $row->OBJECT;
            $data_o->level = $row->LEVEL;
            $data_o->category = $row->CATEGORY;
            $data_o->language = $row->LANG;
            $data_o->hint_url = "https://" . $row->LANG . ".wikipedia.org/wiki/" . $row->SUBJECT;
            $arr[] = $data_o;
        }
    } else {
        $data_o = new stdClass();
        $data_o->success = false;
        $data_o->error = "This method requires a userid.";
    }
    log_exercise('open', $userid, $data_o->eid, 0,$lang);
    header('content-type: application/json; charset=utf-8');
    //header("access-control-allow-origin: *");
    //header('Access-Control-Allow-Origin: *');
    echo json_encode($data_o);
}

function get_single_exercise($eid) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT DISTINCT a.id,a.level,a.category,a.exercise,a.relation,a.subject,a.object FROM tbl_exercises as a where a.id=" . $eid;


    //echo $query;
    $rs = $db->Execute($query);
    $arr = array();
    while ($row = $rs->FetchNextObject()) {
        $data_o = new stdClass();
        $data_o->eid = intval($row->ID);
        $data_o->userid = $userid;
        $data_o->exercise = $row->EXERCISE;
        $data_o->relation = $row->RELATION;
        $data_o->subject = $row->SUBJECT;
        $data_o->object = $row->OBJECT;
        $data_o->level = $row->LEVEL;
        $data_o->category = $row->CATEGORY;
    }

    return $data_o;
}

function prepare_experiment($emethod = "random", $num_words = 1) {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT a.id,a.filter_object,a.remove_for_test FROM tbl_exercises as a where a.id>0";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        switch ($emethod) {
            case 'random': {
                    $vals_test_filter = $row->FILTER_OBJECT;
                    $vals_test = $row->REMOVE_FOR_TEST;
                    $vals_test_arr = explode("|", $vals_test);
                    $vals_test_filter_arr = explode("|", $vals_test_filter);
                    $vals_arr = array_merge($vals_test_arr, $vals_test_filter_arr);
                    $rand_keys = array_rand($vals_arr, $num_words);
                    $list_of_random_answers = array();
                    foreach ($rand_keys as $rand_key) {
                        $list_of_random_answers[] = $vals_arr[$rand_key];
                    }
                    $updated_filtered = array_diff($vals_arr, $list_of_random_answers);
                    $updated_filtered_str = implode("|", $updated_filtered);
                    $list_of_random_answers_str = implode("|", $list_of_random_answers);
                    //Now update the exercise
                    $upd_query = "UPDATE tbl_exercises SET filter_object='" . $updated_filtered_str . "',remove_for_test='" . $list_of_random_answers_str . "' where id=" . $row->ID;
                    $rs1 = $db->Execute($upd_query);
                    break;
                }
        }
    }
}

?>
