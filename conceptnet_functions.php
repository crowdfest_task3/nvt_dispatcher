<?php

function get_conceptnet_objects_all_for($relation = "Synonym", $subject_c = "", $maxConceptNetPages = 1000, $type = "edges") {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    ini_set('max_execution_time', 300000);
    $maxPages = $maxConceptNetPages;



    $offset = 1000;  //Conceptnet has a limitation of 1000 records in its API, we use the offeset
    $counter = 0;
    $fresults = array();

    do {

        if (!isset($nextpage)) {
            $c_api = "http://api.conceptnet.io/query?node=" . $subject_c . "&rel=/r/" . $relation . "&offset=0&limit=1000";
        } else {

            $c_api = "http://api.conceptnet.io" . $nextpage;
        }

        $data = json_decode(file_get_contents(urldecode($c_api)));

        $nextpage = $data->view->nextPage;

        if ($data->error->status != "") {
            echo "Error for: " . $c_api;
        } else {

            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {

                        $arg1 = $edge->start->label;
                        $arg1_c = $edge->start->term;
                        $arg1_lang = $edge->start->language;
                        $arg2 = $edge->end->label;
                        $arg2_c = $edge->end->term;
                        $arg2_lang = $edge->end->language;
                        $relation = $edge->rel->label;
                        $sentence = $edge->surfaceText;

                        if ($type === "edges") {
                            if (($arg1_lang === $arg2_lang) && $arg1_lang === "en") {
                                if ($arg1_c === $subject_c) {
                                    $fresults[] = $arg2;
                                } else {
                                    $fresults[] = $arg1;
                                }
                            }
                        } else {
                            if (($arg1_lang === $arg2_lang) && $arg1_lang === "en") {
                                $fresults[] = $sentence;
                            }
                        }
                    }
                }
            }
            $counter = $offset + $counter;
        }
    } while (($counter < $maxPages) && (isset($nextpage)) && $nextpage !== "");
    $fresults_non_duplicates = array_unique($fresults);
    return implode("|", $fresults_non_duplicates);
}

function process_relatedto_relatedness() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,subject_c,random_object FROM tbl_relatedto where id>0";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $node1 = $row->SUBJECT_C;
        $node2 = "/c/en/" . str_replace(" ", "_", $row->RANDOM_OBJECT);


        $c_api = "http://api.conceptnet.io/relatedness?node1=" . $node1 . "&node2=" . $node2;


        $data = json_decode(file_get_contents(urldecode($c_api)));
//print_r($data);
        $val = $data->value;
        //echo $val;
        $query1 = "UPDATE tbl_relatedto SET relatedness='" . $val . "' where id=" . $row->ID;
        $rs1 = $db->Execute($query1);
        sleep(2);
    }
}

function process_relatedto() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,rest_objects as objs FROM tbl_relatedto where id>0";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $objs = explode("|", $row->OBJS);
        $randIndex = array_rand($objs);
        $element = $objs[$randIndex];
        $query1 = "UPDATE tbl_relatedto SET random_object='" . $element . "' where id=" . $row->ID;
        $rs1 = $db->Execute($query1);
    }
}

function cambridge_retrieve_content_check_relation_exist() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word,conceptnet,objects_not_related_debug FROM tbl_cambridge where id>0";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $filter_object = explode("|", $row->OBJECTS_NOT_RELATED_DEBUG);
        $objects_no_relation = array();
        echo "ID=" . $row->ID . "<br />";
        foreach ($filter_object as $fobj) {

            $obj_array = explode("{", $fobj);
            $fobj_element = $obj_array[0];
            $fobj_c = str_replace(" ", "_", $fobj_element);
            $fobj_c = "/c/en/" . $fobj_c;
            $exist = check_relation_exist($fobj_c, $row->CONCEPTNET, "/r/RelatedTo");
            if (!$exist) {
                $objects_no_relation[] = $fobj_element;
            }
        }
        $filtered_content = implode("|", $objects_no_relation);
        $filtered_content = str_replace("'", "", $filtered_content);
        $query1 = "UPDATE tbl_cambridge SET objects_no_relation_exist='" . $filtered_content . "' where id=" . $row->ID;
        echo "<pre><h2>CONTENT</h2><br />" . $filtered_content . "</pre>";
        $rs1 = $db->Execute($query1);
        sleep(1);
    }
}

function cambridge_retrieve_content_process_relatedness() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word,level,conceptnet,intermediate FROM tbl_cambridge where id>2025";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $items = explode("|", $row->INTERMEDIATE);
        //First randomize the array
        shuffle($items);
        //Then get the 5 first words
        $items_rand = array_slice($items, 0, 25);
        $array_pass = array();
        $array_debug = array();
        foreach ($items_rand as $item) {
            $node1 = $row->CONCEPTNET;
            $node2 = "/c/en/" . str_replace(" ", "_", $item);


            $c_api = "http://api.conceptnet.io/relatedness?node1=" . $node1 . "&node2=" . $node2;


            $data = json_decode(file_get_contents(urldecode($c_api)));
//print_r($data);
            $val = $data->value;
            if ($val < 0.5) {
                $array_pass[] = $item;
            }
            $array_debug[] = $item . "{" . $val . "}";

            sleep(2);
        }
        $query1 = "UPDATE tbl_cambridge SET objects_not_related='" . implode("|", $array_pass) . "',objects_not_related_debug='" . implode("|", $array_debug) . "' where id=" . $row->ID;
        $rs1 = $db->Execute($query1);
    }
}

function cambridge_retrieve_content_intermediate() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word,level,conceptnet,filter_object FROM tbl_cambridge where id>1947";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $filter_object = explode("|", $row->FILTER_OBJECT);
        $content_intermediate = array();
        //First randomize the array
        shuffle($filter_object);
        //Then get the 5 first words
        $filter_obj_rand = array_slice($filter_object, 0, 5);
        foreach ($filter_obj_rand as $fobj) {
            $fobj_c = str_replace(" ", "_", $fobj);
            $fobj_c = "/c/en/" . $fobj_c;
            $content = get_conceptnet_objects_relation($fobj_c, "/r/RelatedTo", 1000);
            //print_r($content);
            $content_intermediate = array_merge($content_intermediate, $content->filtered);
        }
        $filtered_content = implode("|", $content_intermediate);
        $filtered_content = str_replace("'", "", $filtered_content);
        $query1 = "UPDATE tbl_cambridge SET intermediate='" . $filtered_content . "' where id=" . $row->ID;
        echo "Currently @ " . $row->ID . "<br />";
        $rs1 = $db->Execute($query1);
    }
}

function cambridge_retrieve_content() {
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    $query = "SELECT id,word,level,conceptnet FROM tbl_cambridge where id=1574";
    $rs = $db->Execute($query);

    while ($row = $rs->FetchNextObject()) {
        $content = get_conceptnet_objects_relation($row->CONCEPTNET, "/r/RelatedTo", 10000);
        //print_r($content);
        $filtered_content = implode("|", $content->filtered);
        $filtered_content = str_replace("'", "", $filtered_content);
        $unfiltered_content = implode("|", $content->unfiltered);
        $unfiltered_content = str_replace("'", "", $unfiltered_content);
        $exe = "Name one thing that is related to " . $row->WORD;
        $query1 = "UPDATE tbl_cambridge SET relation='RelatedTo',subject='" . $row->WORD . "',exercise='" . $exe . "',object='" . $unfiltered_content . "',filter_object='" . $filtered_content . "' where id=" . $row->ID;
        //echo $query1;
        $rs1 = $db->Execute($query1);
    }
}

function get_conceptnet_objects_relation($subject_c = "", $relation_c = "/r/RelatedTo", $maxConceptNetPages = 1000) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    ini_set('max_execution_time', 300000);
    $maxPages = $maxConceptNetPages;



    $offset = 1000;  //Conceptnet has a limitation of 1000 records in its API, we use the offeset
    $counter = 0;
    $objects = array();
    $objects_unfiltered = array();

    do {
        //http://api.conceptnet.io/query?node=/c/en/cat&rel=/r/RelatedTo&limit=1000
        if (!isset($nextpage)) {
            $c_api = "http://api.conceptnet.io/query?node=" . $subject_c . "&rel=" . $relation_c . "&offset=0&limit=1000";
        } else {
            $c_api = "http://api.conceptnet.io" . $nextpage;
        }

        $data = json_decode(file_get_contents(urldecode($c_api)));

        $nextpage = $data->view->nextPage;

        if ($data->error->status != "") {
            echo "Error for: " . $c_api;
        } else {

            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {

                        $arg1 = $edge->start->label;
                        $arg1_c = $edge->start->term;
                        $arg1_lang = $edge->start->language;
                        $arg2 = $edge->end->label;
                        $arg2_c = $edge->end->term;
                        $arg2_lang = $edge->end->language;
                        $relation = $edge->rel->label;
                        if ($subject_c === $arg1_c) {
                            $proper = $arg2;
                        } else {
                            $proper = $arg1;
                        }


                        $objects_unfiltered[] = $proper;
                        if (($arg1_lang === $arg2_lang) && $arg1_lang === "en" && $relation === "RelatedTo") {
                            $objects[] = $proper;
                        }
                    }
                }
            }
            $counter = $offset + $counter;
        }
    } while (($counter < $maxPages) && (isset($nextpage)) && $nextpage !== "");
    $res = new stdClass();
    $res->filtered = $objects;
    $res->unfiltered = $objects_unfiltered;
    return $res;
}

function check_relation_exist($subject_c1 = "", $subject_c2 = "", $relation_c = "/r/RelatedTo") {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    ini_set('max_execution_time', 300000);

    $counter = 0;

    $c_api = "http://api.conceptnet.io/query?node=" . $subject_c1 . "&other=" . $subject_c2 . "&rel=" . $relation_c;

    $data = json_decode(file_get_contents(urldecode($c_api)));



    if ($data->error->status != "") {
        echo "Error for: " . $c_api;
    } else {

        foreach ($data as &$edges) {
            if (is_array($edges)) {
                foreach ($edges as $edge) {

                    $counter++;
                }
            }
        }
    }

    if ($counter > 0) {
        return false;
    } else {
        return true;
    }
}

function get_conceptnet_objects($subject_c = "", $maxConceptNetPages = 1000) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    ini_set('max_execution_time', 300000);
    $maxPages = $maxConceptNetPages;



    $offset = 1000;  //Conceptnet has a limitation of 1000 records in its API, we use the offeset
    $counter = 0;
    $objects = array();

    do {

        if (!isset($nextpage)) {
            $c_api = "http://api.conceptnet.io" . $subject_c . "?offset=0&limit=1000";
        } else {

            $c_api = "http://api.conceptnet.io" . $nextpage;
        }

        $data = json_decode(file_get_contents(urldecode($c_api)));

        $nextpage = $data->view->nextPage;

        if ($data->error->status != "") {
            echo "Error for: " . $c_api;
        } else {

            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {

                        $arg1 = $edge->start->label;
                        $arg1_c = $edge->start->term;
                        $arg1_lang = $edge->start->language;
                        $arg2 = $edge->end->label;
                        $arg2_c = $edge->end->term;
                        $arg2_lang = $edge->end->language;
                        $relation = $edge->rel->label;
                        if ($subject_c === $arg1_c) {
                            $proper = $arg2;
                        } else {
                            $proper = $arg1;
                        }


                        //
                        if (($arg1_lang === $arg2_lang) && $arg1_lang === "en" && $relation === "RelatedTo") {
                            $objects[] = $proper;
                        }
                    }
                }
            }
            $counter = $offset + $counter;
        }
    } while (($counter < $maxPages) && (isset($nextpage)) && $nextpage !== "");
    return $objects;
}

function get_conceptnet_objects_all($relation = "RelatedTo", $maxConceptNetPages = 1000) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $cfg = new sc_config();
    $db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
    $db->PConnect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
    $db->execute("set names 'utf8'");
    $db->debug = $cfg->db_debug;

    ini_set('max_execution_time', 300000);
    $maxPages = $maxConceptNetPages;



    $offset = 1000;  //Conceptnet has a limitation of 1000 records in its API, we use the offeset
    $counter = 0;


    do {

        if (!isset($nextpage)) {
            $c_api = "http://api.conceptnet.io/r/" . $relation . "?offset=0&limit=1000";
        } else {

            $c_api = "http://api.conceptnet.io" . $nextpage;
        }

        $data = json_decode(file_get_contents(urldecode($c_api)));

        $nextpage = $data->view->nextPage;

        if ($data->error->status != "") {
            echo "Error for: " . $c_api;
        } else {

            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {

                        $arg1 = $edge->start->label;
                        $arg1_c = $edge->start->term;
                        $arg1_lang = $edge->start->language;
                        $arg2 = $edge->end->label;
                        $arg2_c = $edge->end->term;
                        $arg2_lang = $edge->end->language;
                        $relation = $edge->rel->label;

                        $ins2['relation'] = $relation;
                        $ins2['subject'] = conceptnet_argument_to_normal_text($arg1);
                        $ins2['subject_c'] = $arg1_c;
                        $ins2['object'] = conceptnet_argument_to_normal_text($arg2);
                        $ins2['object_c'] = $arg2_c;
                        $ins2['timestamp'] = time();

                        //
                        if (($arg1_lang === $arg2_lang) && $arg1_lang === "en") {
                            //Get all extra
                            $extra_objects = get_conceptnet_objects($arg2_c, 3000);
                            $ins2['rest_objects'] = implode("|", $extra_objects);
                            $db->Autoexecute('tbl_relatedto', $ins2, 'INSERT');
                        }
                    }
                }
            }
            $counter = $offset + $counter;
        }
    } while (($counter < $maxPages) && (isset($nextpage)) && $nextpage !== "");
}

function conceptnet_search_for_proper_uri($countryname, $lang = 'en') {
    //$c_api = "http://conceptnet5.media.mit.edu/data/5.4/uri?language=en&text=" . urlencode(utf8_encode($countryname));
    //New URL for conceptnet 5.6
    $c_api = "http://api.conceptnet.io/uri?language=" . $lang . "&text=" . urlencode(($countryname));
    $data = json_decode(file_get_contents($c_api), true);
//print_r($data);
//echo $data['@id'];
    //return $data['@context']->uri;
    return $data['@id'];
}

function conceptnet_argument_to_normal_text($argument) {
    $t = explode("/", $argument);
    $num = count($t);
    if ($t[$num - 1] === "n") {
        $selected = $t[$num - 2];
    } else {
        $selected = $t[$num - 1];
    }
    return trim(str_replace("_", " ", $selected));
}
