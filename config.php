<?php
/**
 * @package     Nameless Vocabulary Trainer
 * @copyright	Copyright (C) 2019 TASK 3 CROWDFEST TEAM. All rights reserved.
 */
class sc_config {

    var $appurl = "http://nvt.localhost/";
    
    var $temp_folder = "";
    var $reasoner_system_folder = "";
    var $system_folder = "";
    var $db_debug = 0;
    var $code_debug = 0;
    var $show_php_errors = 1;

    //======== Database Configuration =============
    var $db_type = "mysqli";
    var $db_host = "localhost";
    var $db_user = "vtrel-api";
    var $db_pass = "Vtrel-api1!";
    var $db_name = "vtrel";
    
    function sc_config() {
        $this->__construct;
    }

    function __construct() {

        //SETUP Settings for environment paths
        $this->temp_folder = dirname(__FILE__) . "\\tmp\\";
        $this->system_folder = dirname(__FILE__) . "/";
    }


}

?>
