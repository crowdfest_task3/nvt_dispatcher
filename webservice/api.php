<?php

/**
 * @package     Nameless Vocabulary Trainer
 * @copyright	Copyright (C) 2019 TASK3 CROWDFEST TEAMs. All rights reserved.
 */
require_once ('../adodb5/adodb.inc.php');
require_once ('../config.php');
require_once ('../functions.php');
//require_once ('../auth_config.php');
//Initialize the database
$cfg = new sc_config();
$db = ADONewConnection($cfg->db_type); # eg. 'mysql' or 'oci8'
$db->Connect($cfg->db_host, $cfg->db_user, $cfg->db_pass, $cfg->db_name);
$db->execute("set names 'utf8'");
$db->debug = $cfg->db_debug;

//Toggle PHP show errors and notices
if ($cfg->show_php_errors) {
    error_reporting(error_reporting() & ~E_NOTICE);
    ini_set('display_errors', 'on');
} else {
    ini_set('display_errors', 'off');
}
//First get the method parameter FROM REQUEST (STRING)
if (isset($_REQUEST['method'])) {
    $method = strip_tags($_REQUEST['method']);
} else {
    $method = "";
}

//Get the userid parameter FROM REQUEST (INT)
if (isset($_REQUEST['userid'])) {
    $userid = strip_tags($_REQUEST['userid']);
} else {
    $userid = "";
}
//Get the exercise id parameter FROM REQUEST (INT)
if (isset($_REQUEST['eid'])) {
    $eid = strip_tags($_REQUEST['eid']);
} else {
    $eid = "";
}
//Get the exercise response parameter FROM REQUEST (STRING)
if (isset($_REQUEST['response'])) {
    $response = strip_tags($_REQUEST['response']);
} else {
    $response = "";
}
//Get the exercise TYPE parameter FROM REQUEST (STRING)
if (isset($_REQUEST['etype'])) {
    $etype = strip_tags($_REQUEST['etype']);
} else {
    //$etype = "RelatedTo";
    //Changed for Romanian experiment
    $etype = "Synonym";
}
//Get the exercise level parameter FROM REQUEST (STRING)
if (isset($_REQUEST['elevel'])) {
    $elevel = strip_tags($_REQUEST['elevel']);
} else {
    $elevel = "";
}
if (isset($_REQUEST['uname'])) {
    $uname = strip_tags($_REQUEST['uname']);
} else {
    $uname = "";
}
if (isset($_REQUEST['apikey'])) {
    $apikey = strip_tags($_REQUEST['apikey']);
} else {
    $apikey = "";
}
if (isset($_REQUEST['emethod'])) {
    $emethod = strip_tags($_REQUEST['emethod']);
} else {
    $emethod = "";
}
if (isset($_REQUEST['num_words'])) {
    $num_words = strip_tags($_REQUEST['num_words']);
} else {
    $num_words = "";
}
if (isset($_REQUEST['lang'])) {
    $lang = strtolower(strip_tags($_REQUEST['lang']));
} else {
    //$lang = "en";
    //Changed for Romanian experiment
    $lang = "ro";
}
if (isset($_REQUEST['subprocess'])) {
    $subprocess = strip_tags($_REQUEST['subprocess']);
} else {
    $subprocess = "";
}

if (isset($_REQUEST['language_exercise'])) {
    $language_exercise = strtolower(strip_tags($_REQUEST['language_exercise']));
} else {
    $language_exercise = "en";
}
if (isset($_REQUEST['language_interface'])) {
    $language_interface = strtolower(strip_tags($_REQUEST['language_interface']));
} else {
    $language_interface = "en";
}

if (isset($_REQUEST['telegram_id'])) {
    $telegram_id = strtolower(strip_tags($_REQUEST['telegram_id']));
} else {
    $telegram_id = "";
}

if (isset($_REQUEST['telegram_userdata'])) {
    $telegram_userdata = $_REQUEST['telegram_userdata'];
} else {
    $telegram_userdata = "";
}


switch ($method) {
    /* case 'update_dataset_with_timestamp':{
      update_dataset_with_timestamp();
      break;
      } */
    case 'calculate_accuracy_closed': {
            calculate_accuracy_closed();
            break;
        }
    case 'analyze_closed_exercises_results': {
            analyze_closed_exercises_results();
            break;
        }
    case 'process_phrasal_verbs': {
            process_phrasal_verbs($subprocess);
            break;
        }
    case 'filter_cambridge_dataset': {
            filter_cambridge_dataset();
            break;
        }
    case 'cambridge_retrieve_content_check_relation_exist': {
            cambridge_retrieve_content_check_relation_exist();
            break;
        }
    case 'cambridge_retrieve_content_process_relatedness': {
            cambridge_retrieve_content_process_relatedness();
            break;
        }
    case 'cambridge_retrieve_content_intermediate': {
            cambridge_retrieve_content_intermediate();
            break;
        }
    case 'cambridge_retrieve_content': {
            cambridge_retrieve_content();
            break;
        }
    case 'cambridge_check_conceptnet': {
            cambridge_check_conceptnet();
            break;
        }
    case 'process_relatedto_relatedness': {
            process_relatedto_relatedness();
            break;
        }
    case 'process_relatedto': {
            process_relatedto();
            break;
        }
    case 'gather_relatedto': {
            get_conceptnet_objects_all("RelatedTo", 4000);
            break;
        }
    case 'choose_exercise': {
            $active_user = get_user_info($userid, 'internal');
            choose_exercise($userid,$active_user[0]->language_exercise);
            break;
        }
    case 'get_close_exercise': {
            $active_user = get_user_info($userid, 'internal');
            get_close_exercise($userid, $elevel, $etype, $active_user[0]->language_exercise);
            break;
        }
    case 'get_exercise': {
            $active_user = get_user_info($userid, 'internal');
            get_exercise($userid, $elevel,$etype, $active_user[0]->language_exercise);
            break;
        }
        case 'get_exercise_languages': {
            get_exercise_languages();
            break;
        }
    case 'store_response': {
            store_response($userid, $eid, $response);
            break;
        }
    case 'store_close_response': {
            store_close_response($userid, $eid, $response);
            break;
        }
    case 'get_points': {
            get_points($userid);
            break;
        }
    case 'get_notifications': {
            get_notifications($userid);
            break;
        }
    case 'get_leaderboard': {
            get_leaderboard($limit);
            break;
        }
    case 'add_user': {
            add_user($userid, $uname, $language_exercise, $language_interface);
            break;
        }
    case 'update_user': {
            update_user($userid, $uname, $language_exercise, $language_interface);
            break;
        }
    case 'get_user_info': {
            get_user_info($userid, "external");
            break;
        }
    case 'get_random_response': {
            get_random_response($eid, $userid);
            break;
        }
    case 'reset_db': {
            reset_db($apikey);
            break;
        }
    case 'prepare_experiment': {
            prepare_experiment($emethod, $num_words);
            break;
        }
    case 'analyze_conceptnet_results': {
            analyze_conceptnet_results();
            break;
        }
    case 'telegram_add_userdata': {
            telegram_add_userdata($telegram_id,$telegram_userdata);
            break;
        }
    case 'telegram_record_userdata': {
            telegram_record_userdata($telegram_id,$telegram_userdata);
            break;
        }
    case 'telegram_get_userdata': {
            telegram_get_userdata($telegram_id);
            break;
        }
    default: echo 'method not recognized';
        break;
}
?>
